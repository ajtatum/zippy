﻿using System;

namespace zippy_models
{
    public class QueuedMessage
    {
        public string ID { get; set; }
        public string FromAddress { get; set; }
        public string ToAddresses { get; set; }
        public string CCAddresses { get; set; }
        public string BCCAddresses { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateSent { get; set; }
        public string Status { get; set; }
        public int TryCount { get; set; }
    }

    public enum QueuedMessageStatus
    {
        Sent,
        Pending,
        Processing,
        Error,
        ValidationError,
        UnableToSend
    }
}
