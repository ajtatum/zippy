﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zippy_models
{
    public class MailerMessage
    {
        public MailerMessage() { }
        public MailerMessage(string fromAddress, List<string> toAddresses, List<string> ccAddresses,
            List<string> bccAddresses, string subject, string body)
        {
            FromAddress = fromAddress;
            ToAddresses = toAddresses;
            CCAddresses = ccAddresses;
            BCCAddresses = bccAddresses;
            Subject = subject;
            Body = body;
        }

        public string FromAddress { get; set; }
        public List<string> ToAddresses { get; set; }
        public List<string> CCAddresses { get; set; }
        public List<string> BCCAddresses { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
