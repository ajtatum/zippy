﻿using System.Data.Entity.ModelConfiguration;
using zippy_models;

namespace zippy_common.Mapping
{
    public class QueuedMessageMap : EntityTypeConfiguration<QueuedMessage>
    {
        public QueuedMessageMap()
        {
            this.HasKey(x => x.ID);

            this.ToTable("global_email_queue", "dbo");
            this.Property(x => x.ID).HasColumnName("ID");
            this.Property(x => x.FromAddress).HasColumnName("FromAddress");
            this.Property(x => x.ToAddresses).HasColumnName("ToAddresses");
            this.Property(x => x.CCAddresses).HasColumnName("CCAddresses");
            this.Property(x => x.BCCAddresses).HasColumnName("BCCAddresses");
            this.Property(x => x.Subject).HasColumnName("Subject");
            this.Property(x => x.Body).HasColumnName("Body");
            this.Property(x => x.DateCreated).HasColumnName("DateCreated");
            this.Property(x => x.DateSent).HasColumnName("DateSent");
            this.Property(x => x.Status).HasColumnName("Status");
            this.Property(x => x.TryCount).HasColumnName("TryCount");
        }
    }
}