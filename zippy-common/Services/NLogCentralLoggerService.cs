﻿using System;
using System.Collections.Generic;
using NLog;
using System.Web;
using System.IO;

namespace zippy_common.Services
{
    public class NLogCentralLoggerService : ICentralLogger
    {
        private readonly ILogger logger;

        public NLogCentralLoggerService(ILogger logger)
        {
            this.logger = logger;
        }

        public void Debug(string message, Exception e = null)
        {
            Log(LogLevel.Debug, message, e);
        }

        public void Error(string message, Exception e = null)
        {
            Log(LogLevel.Error, message, e);
        }

        public void Fatal(string message, Exception e = null)
        {
            Log(LogLevel.Fatal, message, e);
        }

        public void Info(string message, Exception e = null)
        {
            Log(LogLevel.Info, message, e);
        }

        public void Trace(string message, Exception e = null)
        {
            Log(LogLevel.Trace, message, e);
        }

        public void Warn(string message, Exception e = null)
        {
            Log(LogLevel.Warn, message, e);
        }

        private void Log(LogLevel level, string message, Exception e)
        {
            var requestBody = "";
            try
            {
                requestBody = GetRequestBody(System.Web.HttpContext.Current.Request);
            }
            catch { }

            //Do this to send the System.Exception.ToString() output to NLog.
            //Normally, we'd send the System.Exception object straight to NLog and let ${exception:format=tostring} handle this for us,
            //But the NLog ElasticSearch.Net target is dumb and sends a serialized exception object anyway, which creates a new type of document and
            //makes our ES server reject the POST.
            LogEventInfo ev = new LogEventInfo(level, logger.Name, message);
            ev.Properties["exceptiontostring"] = e != null ? e.ToString() : string.Empty;
            ev.Properties["requestBody"] = requestBody;

            logger.Log(ev);
            return;
        }

        private static string GetRequestBody(HttpRequest r)
        {
            Stream req = r.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            StreamReader s = new StreamReader(req);
            return s.ReadToEnd();
        }
    }
}