﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace zippy_common.Services
{
    public interface IAppSettings
    {
        T GetSetting<T>(string name);
    }
}