﻿namespace zippy_common.Services
{
    public interface ICentralLogger
    {
        void Debug(string message, System.Exception e = null);
        void Error(string message, System.Exception e = null);
        void Fatal(string message, System.Exception e = null);
        void Info(string message, System.Exception e = null);
        void Trace(string message, System.Exception e = null);
        void Warn(string message, System.Exception e = null);
    }
}