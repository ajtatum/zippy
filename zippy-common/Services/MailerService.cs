﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using NLog.Internal;
using zippy_common.DAL;
using zippy_models;

namespace zippy_common.Services
{
    public class MailerService : IMailer
    {
        private readonly MailerDbContext mailerDbContext;
        private readonly IAppSettings settings;

        public MailerService(MailerDbContext mailerDbContext, IAppSettings settings)
        {
            this.mailerDbContext = mailerDbContext;
            this.settings = settings;
        }

        private bool ValidateMessage(MailerMessage mailerMessage)
        {
            BuildMessage(mailerMessage);
            return true;
        }

        private MailMessage BuildMessage(MailerMessage mailerMessage)
        {
            MailMessage message = new MailMessage();
            string validationAddress = "";

            try
            {
                validationAddress = "From:" + mailerMessage.FromAddress;
                message.From = new MailAddress(mailerMessage.FromAddress);

                if (mailerMessage.ToAddresses.Any())
                {
                    foreach (var mailAddress in mailerMessage.ToAddresses)
                    {
                        validationAddress = "To:" + mailAddress;
                        message.To.Add(new MailAddress(mailAddress));
                    }
                }

                if (mailerMessage.CCAddresses.Any())
                {
                    foreach (var mailAddress in mailerMessage.CCAddresses)
                    {
                        validationAddress = "Cc:" + mailAddress;
                        message.CC.Add(new MailAddress(mailAddress));
                    }
                }

                if (mailerMessage.BCCAddresses.Any())
                {
                    foreach (var mailAddress in mailerMessage.BCCAddresses)
                    {
                        validationAddress = "Bcc:" + mailAddress;
                        message.Bcc.Add(new MailAddress(mailAddress));
                    }
                }

                message.Subject = mailerMessage.Subject;
                message.Body = mailerMessage.Body;

                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(mailerMessage.Body, Encoding.UTF8, "text/html");
                message.AlternateViews.Add(htmlView);
                message.IsBodyHtml = true;
            }
            catch (FormatException ex)
            {
                ex.Data.Add("Zippy.ValidationAddress", validationAddress);
                throw;
            }
            catch (ArgumentException ex)
            {
                ex.Data.Add("Zippy.ValidationAddress", validationAddress);
                throw;
            }

            return message;
        }

        public async Task<bool> SendMessage(MailerMessage mailerMessage)
        {
            using (SmtpClient client = new SmtpClient())
            {
                var message = BuildMessage(mailerMessage);
                await client.SendMailAsync(message);
            }
            return true;
        }

        public async Task<int> QueueMessage(MailerMessage mailerMessage)
        {
            if (ValidateMessage(mailerMessage))
            {

                var queuedMessage = new QueuedMessage();
                queuedMessage.ID = Guid.NewGuid().ToString();
                queuedMessage.FromAddress = mailerMessage.FromAddress;

                queuedMessage.ToAddresses = AddressStringListToString(mailerMessage.ToAddresses);
                queuedMessage.CCAddresses = AddressStringListToString(mailerMessage.CCAddresses);
                queuedMessage.BCCAddresses = AddressStringListToString(mailerMessage.BCCAddresses);

                queuedMessage.Subject = mailerMessage.Subject;
                queuedMessage.Body = mailerMessage.Body;
                queuedMessage.DateCreated = DateTime.Now;
                queuedMessage.DateSent = null;
                queuedMessage.Status = QueuedMessageStatus.Pending.ToString();
                queuedMessage.TryCount = 0;

                mailerDbContext.GlobalEmailQueue.Add(queuedMessage);
                return await mailerDbContext.SaveChangesAsync();
            }

            return 0;
        }

        public List<QueuedMessage> GetQueuedMessages()
        {
            var statuses = new List<string>() { QueuedMessageStatus.Pending.ToString(), QueuedMessageStatus.Error.ToString() };
            var maxTryCount = settings.GetSetting<Int32>("MaxTryCount");

            var messages = mailerDbContext.GlobalEmailQueue
                .Where(x => x.DateSent == null && statuses.Contains(x.Status) && x.TryCount < maxTryCount)
                .OrderBy(x => x.DateCreated)
                .ToList();

            messages.ForEach(m =>
            {
                m.Status = QueuedMessageStatus.Processing.ToString();
            });
            mailerDbContext.SaveChanges();

            return messages;
        }

        public async Task ProcessQueuedMessage(QueuedMessage message)
        {
            try
            {
                message.TryCount = message.TryCount + 1;

                MailerMessage mailMessage = new MailerMessage();

                mailMessage.FromAddress = message.FromAddress;
                mailMessage.ToAddresses = AddressStringToList(message.ToAddresses);
                mailMessage.CCAddresses = AddressStringToList(message.CCAddresses);
                mailMessage.BCCAddresses = AddressStringToList(message.BCCAddresses);

                mailMessage.Subject = message.Subject;
                mailMessage.Body = message.Body;

                await SendMessage(mailMessage);

                message.DateSent = DateTime.Now;
                message.Status = QueuedMessageStatus.Sent.ToString();

                EditQueuedMessage(message);
            }
            catch(Exception ex)
            {
                if (message.TryCount >= settings.GetSetting<Int32>("MaxTryCount"))
                {
                    message.Status = QueuedMessageStatus.UnableToSend.ToString();
                }
                else
                {
                    message.Status = QueuedMessageStatus.Error.ToString();
                }

                if (ex.Data.Contains("Zippy.ValidationAddress"))
                {
                    message.Status = QueuedMessageStatus.ValidationError.ToString();
                }

                message.DateSent = null;

                EditQueuedMessage(message);

                throw;
            }

            return;
        }

        public void EditQueuedMessage(QueuedMessage message)
        {
            QueuedMessage dbMessage = mailerDbContext.GlobalEmailQueue.Single(qm => qm.ID == message.ID);
            dbMessage.FromAddress = message.FromAddress;
            dbMessage.ToAddresses = message.ToAddresses;
            dbMessage.CCAddresses = message.CCAddresses;
            dbMessage.BCCAddresses = message.BCCAddresses;
            dbMessage.Subject = message.Subject;
            dbMessage.Body = message.Body;
            dbMessage.DateCreated = message.DateCreated;
            dbMessage.DateSent = message.DateSent;
            dbMessage.Status = message.Status;
            dbMessage.TryCount = message.TryCount;
            mailerDbContext.SaveChanges();
        }

        public async Task<int> TruncateMessages(double messageAgeMinutes)
        {
            var cutoffDate = DateTime.Now.AddMinutes(-1 * messageAgeMinutes);
            var messages = mailerDbContext.GlobalEmailQueue
                .Where(x => x.DateCreated < cutoffDate && x.Status == QueuedMessageStatus.Sent.ToString())
                .ToList();

            messages.ForEach(m =>
            {
                mailerDbContext.GlobalEmailQueue.Remove(m);
            });
            await mailerDbContext.SaveChangesAsync();

            return messages.Count();
        }

        private string AddressStringListToString(List<string> list)
        {
            MailAddressCollection addresses = new MailAddressCollection();
            list.ForEach(a => addresses.Add(new MailAddress(a)));
            return addresses.ToString();
        }

        private List<string> AddressStringToList(string str)
        {
            MailAddressCollection addresses = new MailAddressCollection();

            if(!String.IsNullOrEmpty(str))
            {
                addresses.Add(str);
            }
            
            return addresses.Select(a => a.ToString()).ToList();
        }
    }
}