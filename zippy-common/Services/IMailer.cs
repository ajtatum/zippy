﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using zippy_models;

namespace zippy_common.Services
{
    public interface IMailer
    {
        Task<bool> SendMessage(MailerMessage mailerMessage);
        Task<int> QueueMessage(MailerMessage mailerMessage);
        List<QueuedMessage> GetQueuedMessages();
        Task ProcessQueuedMessage(QueuedMessage message);
        void EditQueuedMessage(QueuedMessage message);
        Task<int> TruncateMessages(double messageAgeMinutes);
    }
}