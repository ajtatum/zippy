﻿using System;
using System.Configuration;

namespace zippy_common.Services
{
    public class WebConfigAppSettingsService : IAppSettings
    {
        public T GetSetting<T>(string name)
        {
            var value = ConfigurationManager.AppSettings[name];
            return (T)Convert.ChangeType(value, Type.GetTypeCode(typeof(T)));
        }
    }
}