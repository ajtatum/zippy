﻿using System;
using System.Data.Entity;
using zippy_common.Mapping;
using zippy_models;

namespace zippy_common.DAL
{
    public class MailerDbContext : DbContext, IDisposable
    {
        public MailerDbContext() : base("Name=mailer-database")
        {
            Configuration.LazyLoadingEnabled = false;

            // not sure why we have to do this, but recommended on StackOverflow
            // http://stackoverflow.com/questions/21641435/error-no-entity-framework-provider-found-for-the-ado-net-provider-with-invarian
            // without the below the MailerTest SendMessageToBen fails
            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public virtual IDbSet<QueuedMessage> GlobalEmailQueue { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new QueuedMessageMap());
        }
    }
}