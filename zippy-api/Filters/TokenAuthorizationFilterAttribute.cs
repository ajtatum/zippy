﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;
using Autofac.Integration.WebApi;
using zippy_common.Services;

namespace zippy_api.Filters
{
    public class TokenAuthorizationFilterAttribute : AuthorizationFilterAttribute, IAutofacAuthorizationFilter
    {
        private readonly IAppSettings appSettingsService;

        public TokenAuthorizationFilterAttribute()
        {
            this.appSettingsService = new WebConfigAppSettingsService();
        }

        public TokenAuthorizationFilterAttribute(IAppSettings appSettingsService)
        {
            this.appSettingsService = appSettingsService;
        }

        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            string token;

            try
            {
                token = actionContext.Request.Headers.GetValues("Authorization-Token").First();
            }
            catch (Exception)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest);
                return;
            }

            if (token != appSettingsService.GetSetting<string>("ApiToken"))
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                return;
            }
        }
    }
}