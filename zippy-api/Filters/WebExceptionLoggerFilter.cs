﻿using NLog;
using System.Web.Http.Filters;
using zippy_common.Services;

namespace zippy_api.Filters
{
    //Registered in WebApiConfig.
    public class WebExceptionLoggerFilter : System.Web.Http.Filters.ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            (new NLogCentralLoggerService(LogManager.GetLogger("global"))).Error("", context.Exception);
        }
    }
}