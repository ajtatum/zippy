﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using zippy_api.Filters;
using zippy_common.Services;
using zippy_models;

namespace zippy_api.Controllers
{
    public class MailController : ApiController
    {
        private IMailer mailerService;
        private IAppSettings settingService;

        public MailController(IMailer mailerService, IAppSettings settingService)
        {
            this.mailerService = mailerService;
            this.settingService = settingService;
        }

        public async Task<HttpResponseMessage> Send(MailerMessage mailerMessage)
        {
            if (mailerMessage == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "No mail message was sent.");
            }

            try
            {
                await mailerService.SendMessage(mailerMessage);
            }
            catch (Exception ex) when (ex is FormatException || ex is ArgumentException)
            {
                if (ex.Data.Contains("Zippy.ValidationAddress"))
                {
                    var badAddress = ex.Data["Zippy.ValidationAddress"] as string;
                    var addressParts = badAddress.Split(new char[] { ':' });
                    var response = new Dictionary<string, string>() {
                        { "Status", "BADADDRESS" },
                        { "Address Type", addressParts[0].ToUpper() },
                        { "Address Value", addressParts[1] },
                        { "Message", ex.Message }
                    };
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                throw;
            }

            return Request.CreateResponse(HttpStatusCode.OK, "Send Success");
        }

        public async Task<HttpResponseMessage> Queue(MailerMessage mailerMessage)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            try
            {
                await mailerService.QueueMessage(mailerMessage);
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex.Message);
            }
            catch (Exception ex) when (ex is FormatException || ex is ArgumentException)
            {
                if (ex.Data.Contains("Zippy.ValidationAddress"))
                {
                    var badAddress = ex.Data["Zippy.ValidationAddress"] as string;
                    var addressParts = badAddress.Split(new char[] { ':' });
                    var response = new Dictionary<string, string>() {
                        { "Status", "BADADDRESS" },
                        { "Address Type", addressParts[0].ToUpper() },
                        { "Address Value", addressParts[1] },
                        { "Message", ex.Message }
                    };
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                throw;
            }

            return Request.CreateResponse(HttpStatusCode.Created, mailerMessage);
        }
    }

    [TokenAuthorizationFilter]
    [RoutePrefix("v1/Mail")]
    public class v1_MailController : MailController
    {
        public v1_MailController(IMailer mailerService, IAppSettings settingsService)
            : base(mailerService, settingsService)
        {
        }

        [EnableCors("*", "*", "POST")]
        [Route("Send")]
        [HttpPost]
        public new async Task<HttpResponseMessage> Send(MailerMessage mailerMessage)
        {
            return await base.Send(mailerMessage);
        }

        [EnableCors("*", "*", "POST")]
        [Route("Queue")]
        [HttpPost]
        public new async Task<HttpResponseMessage> Queue(MailerMessage mailerMessage)
        {
            return await base.Queue(mailerMessage);
        }
    }
}
