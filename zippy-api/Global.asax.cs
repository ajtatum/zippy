﻿using System.Reflection;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.WebApi;
using zippy_common.DAL;
using zippy_common.Services;
using SDammann.WebApi.Versioning;
using NLog;

namespace zippy_api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiFilterProvider(GlobalConfiguration.Configuration);

            builder.RegisterType<MailerDbContext>().InstancePerRequest();
            builder.RegisterType<MailerService>().AsImplementedInterfaces().InstancePerRequest();
            builder.RegisterType<NLogCentralLoggerService>().AsImplementedInterfaces().InstancePerRequest();
            builder.RegisterType<WebConfigAppSettingsService>().AsImplementedInterfaces().InstancePerRequest();
            builder.Register(c => LogManager.GetLogger("global")).As<ILogger>();

            var container = builder.Build();
            var resolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = resolver;

            GlobalConfiguration.Configuration.Services.Replace(typeof(IApiExplorer),
                new VersionedApiExplorer(GlobalConfiguration.Configuration)
            );

            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
        }
    }
}
