﻿To send something to the queue send it to http://localhost:64531/v1/Mail/Queue

JSON Example:
{
  "FromAddress": "goodbye@ajtatum.com",
  "ToAddresses": ["hello@ajtatum.com","hi@ajtatum.com"],
  "CCAddresses" : [],
  "BCCAddresses" : [],
  "Subject" : "Test Mailer API 10",
  "Body" : "This is the message body"
}