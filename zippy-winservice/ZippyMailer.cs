﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.ServiceProcess;
using Autofac;
using zippy_common.DAL;
using zippy_common.Services;
using zippy_models;
using NLog;
using System.Threading.Tasks;

namespace zippy_winservice
{
    public partial class ZippyWinservice : ServiceBase
    {
        private IContainer container;
        private System.Timers.Timer mainTimer;
        private System.Timers.Timer truncateTimer;
        private double truncateMessageAgeMinutes = double.MaxValue;

        public ZippyWinservice()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterType<MailerDbContext>().InstancePerDependency();
            builder.RegisterType<MailerService>().AsImplementedInterfaces().InstancePerDependency();
            builder.RegisterType<NLogCentralLoggerService>().AsImplementedInterfaces().InstancePerDependency();
            builder.RegisterType<WebConfigAppSettingsService>().AsImplementedInterfaces().InstancePerDependency();
            builder.Register(c => LogManager.GetLogger("global")).As<ILogger>().InstancePerDependency();
            this.container = builder.Build();

            IAppSettings settings = container.Resolve<IAppSettings>();

            container.Resolve<ICentralLogger>().Trace(string.Format("Starting ZippyWinservice with the {0} config", settings.GetSetting<string>("ConfigInfo")));

            this.mainTimer = new System.Timers.Timer();
            mainTimer.Interval = settings.GetSetting<Double>("MainTimerInterval");
            mainTimer.Elapsed += async (sender, e) => await OnTimer();
            mainTimer.Start();

            this.truncateTimer = new System.Timers.Timer();
            truncateTimer.Interval = settings.GetSetting<Double>("TruncateTimerInterval");
            truncateTimer.Elapsed += async (sender, e) => await OnTruncateTimer();
            truncateTimer.Start();

            this.truncateMessageAgeMinutes = settings.GetSetting<Double>("TruncateMessageAgeMinutes");
        }

        public async Task OnTimer()
        {
            await ProcessMessages(this.container, this.mainTimer);
        }

        public async Task ProcessMessages(IContainer injectedContainer, System.Timers.Timer timer)
        {
            timer.Stop();
            ICentralLogger logger = injectedContainer.Resolve<ICentralLogger>();

            try
            {
                IMailer mailerService = injectedContainer.Resolve<IMailer>();
                IAppSettings settings = injectedContainer.Resolve<IAppSettings>();

                int messageBunchSize = settings.GetSetting<int>("MessageBunchSize");
                int messageBunchWaitMilliseconds = settings.GetSetting<int>("MessageBunchWaitMilliseconds");
                int waitBetweenMessagesMilliseconds = settings.GetSetting<int>("WaitBetweenMessagesMilliseconds");
                int currentBunchMessageCount = 0;

                var messages = mailerService.GetQueuedMessages();

                foreach (var message in messages)
                {
                    try
                    {
                        if(currentBunchMessageCount >= messageBunchSize)
                        {
                            // Wait between bunches of messages
                            System.Threading.Thread.Sleep(messageBunchWaitMilliseconds);
                            currentBunchMessageCount = 0;
                        }
                        else if(waitBetweenMessagesMilliseconds > 0)
                        {
                            // Wait between messages
                            System.Threading.Thread.Sleep(waitBetweenMessagesMilliseconds);
                        }

                        logger.Trace(string.Format("Processing Message {0}", message.ID));
                        await mailerService.ProcessQueuedMessage(message);
                        logger.Trace(string.Format("Processed Message {0}", message.ID));
                        currentBunchMessageCount += 1;
                    }
                    catch (Exception ex)
                    {
                        var additionalInfo = "";
                        if (ex.Data.Contains("Zippy.ValidationAddress"))
                        {
                            var badAddress = ex.Data["Zippy.ValidationAddress"] as string;
                            additionalInfo = string.Format("Bad Address, '{0}'. ", badAddress);
                        }

                        logger.Error(string.Format("Failed to send message {0}. Additional details: {1}", message.ID, additionalInfo), ex);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Process Messages Exception.", ex);
            }
            timer.Start();

        }

        public async Task OnTruncateTimer()
        {
            await TruncateMessages(this.container, this.truncateTimer, this.truncateMessageAgeMinutes);
        }

        public async Task TruncateMessages(IContainer injectedContainer, System.Timers.Timer timer, double messageAgeMinutes)
        {
            timer.Stop();
            ICentralLogger logger = injectedContainer.Resolve<ICentralLogger>();

            try
            {
                IMailer mailerService = injectedContainer.Resolve<IMailer>();

                logger.Trace("Truncating Messages");
                var count = await mailerService.TruncateMessages(messageAgeMinutes);
                logger.Trace(string.Format("Truncated {0} Messages", count));
            }
            catch (Exception ex)
            {
                logger.Error("Truncate Messages Exception.", ex);
            }
            timer.Start();
        }

        protected override void OnStop()
        {
            this.container.Resolve<ICentralLogger>().Trace("Stopping ZippyWinservice");
        }

        protected override void OnContinue()
        {
            this.container.Resolve<ICentralLogger>().Trace("ZippyWinservice OnContinue");
        }
    }
}
