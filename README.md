### Sending a request to the API
To send something to the queue send it to http://localhost:64531/v1/Mail/Queue

JSON Example:
{
  "FromAddress": "do.not.reply@ajtatum.com",
  "ToAddresses": ["andrew.j.tatum@gmail.com","aj@tatum.pro"],
  "CCAddresses" : [],
  "BCCAddresses" : [],
  "Subject" : "Test Mailer API 10",
  "Body" : "This is the message body"
}

### To Install the Service
To install the service, run the following from Command Prompt with Admin rights and from the bin\Debug folder.
C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe zippy-winservice.exe