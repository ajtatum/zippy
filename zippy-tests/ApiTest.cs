﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using zippy_models;
using zippy_api.Controllers;
using zippy_common.Services;
using Moq;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Net.Http;

namespace zippy_tests
{
    [TestClass]
    public class ApiTest
    {
        protected MailController controller;
        protected Mock<IMailer> mailer;
        protected IAppSettings settings;

        [TestInitialize]
        public void Init()
        {
            this.settings = new WebConfigAppSettingsService();
            this.mailer = new Mock<IMailer>();
            this.controller = new MailController(this.mailer.Object, this.settings);
            this.controller.Request = new HttpRequestMessage();
            using (var httpConfiguration = new System.Web.Http.HttpConfiguration())
            {
                this.controller.Request.SetConfiguration(httpConfiguration);
            }
        }

        [TestMethod]
        public async Task QueueMessage()
        {
            await this.controller.Queue(new MailerMessage(
                "do.not.reply@ajtatum.com",
                new List<string> { "hello@ajtatum.com" },
                new List<string>(),
                new List<string>(),
                "Unit Testing",
                "Unit Test Body"
            ));

            this.mailer.Verify(s => s.QueueMessage(It.IsAny<MailerMessage>()));
        }

        [TestMethod]
        public async Task SendMessage()
        {
            await this.controller.Send(new MailerMessage(
                "do.not.reply@ajtatum.com",
                new List<string> { "hello@ajtatum.com" },
                new List<string>(),
                new List<string>(),
                "Unit Testing",
                "Unit Test Body"
            ));

            this.mailer.Verify(s => s.SendMessage(It.IsAny<MailerMessage>()));
        }
    }
}