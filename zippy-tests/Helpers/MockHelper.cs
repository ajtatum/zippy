﻿using Moq;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace zippy_tests.Helpers
{
    internal class MockHelper
    {
        public static Mock<IDbSet<T>> GetMockDbSet<T>(IQueryable<T> queryable) where T : class
        {
            var dbSet = new Mock<IDbSet<T>>();

            dbSet.As<IDbAsyncEnumerable<T>>()
            .Setup(m => m.GetAsyncEnumerator())
            .Returns(new TestDbAsyncEnumerator<T>(queryable.GetEnumerator()));

            dbSet.As<IQueryable<T>>()
            .Setup(m => m.Provider)
            .Returns(new TestDbAsyncQueryProvider<T>(queryable.Provider));

            dbSet.Setup(s => s.Expression).Returns(queryable.Expression);
            dbSet.Setup(s => s.ElementType).Returns(queryable.ElementType);
            dbSet.Setup(s => s.GetEnumerator()).Returns(queryable.GetEnumerator());

            return dbSet;
        }
    }
}