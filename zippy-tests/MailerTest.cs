﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using zippy_models;
using zippy_api.Controllers;
using zippy_common.DAL;
using zippy_common.Services;
using zippy_tests.Helpers;
using Moq;

namespace zippy_tests
{
    [TestClass]
    public class MailerTest
    {
        protected IMailer mailer;
        protected IAppSettings settings;
        protected Mock<MailerDbContext> dbContext;
        protected Mock<IDbSet<QueuedMessage>> dbSet;

        [TestInitialize]
        public void Init()
        {
            this.dbContext = new Mock<MailerDbContext>();
            this.settings = new WebConfigAppSettingsService();
            this.mailer = new MailerService(this.dbContext.Object, this.settings);

            var data = new List<QueuedMessage>
            {
                new QueuedMessage
                {
                    ID = Guid.NewGuid().ToString(),
                    FromAddress = "do.not.reply@ajtatum.com",
                    ToAddresses = "hello@ajtatum.com",
                    CCAddresses = "\"AJ Tatum\" <aj@ajtatum.com>, me@ajtatum.com",
                    BCCAddresses = "",
                    Subject = "Test Subject",
                    Body = "Test Body",
                    DateCreated = DateTime.Now,
                    DateSent = null,
                    Status = QueuedMessageStatus.Pending.ToString(),
                    TryCount = 0
                },
                new QueuedMessage
                {
                    ID = Guid.NewGuid().ToString(),
                    FromAddress = "do.not.reply@ajtatum.com",
                    ToAddresses = "hello@ajtatum.com",
                    CCAddresses = "me@ajtatum.com,\"Tatum, AJ\" <aj@ajtatum.com>",
                    BCCAddresses = "",
                    Subject = "Test Subject 2",
                    Body = "Test Body 2",
                    DateCreated = DateTime.Now,
                    DateSent = null,
                    Status = QueuedMessageStatus.Error.ToString(),
                    TryCount = 1
                },
                new QueuedMessage
                {
                    ID = Guid.NewGuid().ToString(),
                    FromAddress = "do.not.reply@ajtatum.com",
                    ToAddresses = "hello@ajtatum.com",
                    CCAddresses = "aj@ajtatum.com,aj@tatum.pro",
                    BCCAddresses = "",
                    Subject = "Already exhausted attempts",
                    Body = "Test Body 3",
                    DateCreated = DateTime.Now,
                    DateSent = null,
                    Status = QueuedMessageStatus.UnableToSend.ToString(),
                    TryCount = 5
                },
                new QueuedMessage
                {
                    ID = Guid.NewGuid().ToString(),
                    FromAddress = "do.not.reply@ajtatum.com",
                    ToAddresses = "aj@ajtatum.com",
                    CCAddresses = "hello@ajtatum.com,me@ajtatum.com",
                    BCCAddresses = "",
                    Subject = "Old Subject - Sent",
                    Body = "Old Body - Sent",
                    DateCreated = DateTime.Now.AddMinutes(-16),
                    DateSent = DateTime.Now.AddMinutes(-16),
                    Status = QueuedMessageStatus.Sent.ToString(),
                    TryCount = 5
                },
                new QueuedMessage
                {
                    ID = Guid.NewGuid().ToString(),
                    FromAddress = "do.not.reply@ajtatum.com",
                    ToAddresses = "aj@ajtatum.com",
                    CCAddresses = "hello@ajtatum.com,me@ajtatum.com",
                    BCCAddresses = "",
                    Subject = "Old Subject - Error",
                    Body = "Old Body - Error",
                    DateCreated = DateTime.Now.AddMinutes(-16),
                    DateSent = null,
                    Status = QueuedMessageStatus.Error.ToString(),
                    TryCount = 5
                }
            }.AsQueryable();

            this.dbSet = MockHelper.GetMockDbSet<QueuedMessage>(data);
        }

        [TestMethod]
        public async Task SendMessage()
        {
            MailerMessage m = new MailerMessage(
                "do.not.reply@ajtatum.com",
                new List<string> { "ben@ajtatum.com" },
                new List<string>(),
                new List<string>(),
                "Unit Testing",
                "Unit Test Body"
            );

            await this.mailer.SendMessage(m);
        }

        [TestMethod]
        public async Task SendMessagesWithFormattedAddresses()
        {
            MailerMessage m = new MailerMessage(
                "Ben <ben@ajtatum.com>",
                new List<string> { "ben@ajtatum.com" },
                new List<string>(),
                new List<string>(),
                "Unit Testing",
                "Unit Test Body"
            );

            await this.mailer.SendMessage(m);

            m = new MailerMessage(
                "Ben <ben@ajtatum.com>",
                new List<string> { "Ben <ben@ajtatum.com>", "\"Tatum, James\" <james@ajtatum.com>" },
                new List<string>(),
                new List<string>(),
                "Unit Testing",
                "Unit Test Body"
            );

            await this.mailer.SendMessage(m);
        }

        [TestMethod]
        public async Task QueueMessage()
        {
            this.dbContext.Setup(c => c.GlobalEmailQueue).Returns(this.dbSet.Object);

            MailerMessage m = new MailerMessage(
                "do.not.reply@ajtatum.com",
                new List<string> { "ben@ajtatum.com" },
                new List<string>(),
                new List<string>(),
                "Unit Testing",
                "Unit Test Body"
            );

            await this.mailer.QueueMessage(m);

            this.dbSet.Verify(d => d.Add(It.IsAny<QueuedMessage>()));
            this.dbContext.Verify(c => c.SaveChangesAsync());
        }

        [TestMethod]
        public async Task QueueMessagesWithFormattedAddresses()
        {
            this.dbContext.Setup(c => c.GlobalEmailQueue).Returns(this.dbSet.Object);

            MailerMessage m = new MailerMessage(
                "Ben <ben@ajtatum.com>",
                new List<string> { "ben@ajtatum.com" },
                new List<string>(),
                new List<string>(),
                "Unit Testing",
                "Unit Test Body"
            );

            await this.mailer.QueueMessage(m);

            this.dbSet.Verify(d => d.Add(It.IsAny<QueuedMessage>()));
            this.dbContext.Verify(c => c.SaveChangesAsync());

            this.dbContext.Setup(c => c.GlobalEmailQueue).Returns(this.dbSet.Object);

            m = new MailerMessage(
                "Ben <ben@ajtatum.com>",
                new List<string> { "Ben <ben@ajtatum.com>", "\"Tatum, James\" <james@ajtatum.com>" },
                new List<string>(),
                new List<string>(),
                "Unit Testing",
                "Unit Test Body"
            );

            await this.mailer.QueueMessage(m);
            this.dbSet.Verify(d => d.Add(It.IsAny<QueuedMessage>()));
            this.dbContext.Verify(c => c.SaveChangesAsync());
        }

        [TestMethod]
        public void GetQueuedMessages()
        {
            this.dbContext.Setup(c => c.GlobalEmailQueue).Returns(this.dbSet.Object);
            this.dbContext.Setup(c => c.SaveChangesAsync());

            var results = this.mailer.GetQueuedMessages();

            Assert.IsTrue(results.Count() == 2, "Correct messages are retrieved from the queue");
            Assert.IsTrue(results.First().Status == QueuedMessageStatus.Processing.ToString(), "Retrieving message from queue sets status to pending.");
            Assert.IsTrue(!results.Any(qm => qm.Subject == "Already exhausted attempts"), "Does not retrieve messages that have been retried too many times.");
        }

        [TestMethod]
        public async Task ProcessQueuedMessage()
        {
            this.dbContext.Setup(c => c.SaveChanges());
            this.dbContext.Setup(c => c.GlobalEmailQueue).Returns(this.dbSet.Object);

            await this.mailer.ProcessQueuedMessage(this.dbSet.Object.First(x => x.Subject == "Test Subject"));
            this.dbContext.Verify(c => c.SaveChanges(), Times.Exactly(1));

            Assert.IsTrue(this.dbSet.Object.First(x => x.Subject == "Test Subject").TryCount == 1, "Incremented try count on message.");
            Assert.IsTrue(this.dbSet.Object.First(x => x.Subject == "Test Subject").Status == QueuedMessageStatus.Sent.ToString(), "Set Sent status on message.");
        }

        [TestMethod]
        public async Task TruncateMessages()
        {
            this.dbContext.Setup(c => c.GlobalEmailQueue).Returns(this.dbSet.Object);
            var count = await this.mailer.TruncateMessages(15);
            var sentMail = this.dbSet.Object.First(x => x.Subject == "Old Subject - Sent");
            Assert.IsTrue(count == 1);
        }
    }
}