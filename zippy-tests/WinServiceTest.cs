﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using zippy_models;
using zippy_api.Controllers;
using zippy_common.DAL;
using zippy_common.Services;
using zippy_tests.Helpers;
using zippy_winservice;
using Moq;
using Autofac;
using System.ServiceProcess;

namespace zippy_tests
{
    [TestClass]
    public class WinServiceTest
    {
        protected IContainer container;
        protected ZippyWinservice winService;
        protected Mock<IMailer> mailer;
        protected Mock<ICentralLogger> logger;
        protected Mock<IAppSettings> settings;
        private List<QueuedMessage> testData;

        [TestInitialize]
        public void Init()
        {
            this.winService = new ZippyWinservice();

            this.settings = new Mock<IAppSettings>();
            this.mailer = new Mock<IMailer>();
            this.logger = new Mock<ICentralLogger>();

            this.testData = new List<QueuedMessage>
            {
                new QueuedMessage
                {
                    ID = Guid.NewGuid().ToString(),
                    FromAddress = "do.not.reply@ajtatum.com",
                    ToAddresses = "ben@ajtatum.com",
                    CCAddresses = "james@ajtatum.com,bentley@ajtatum.com",
                    BCCAddresses = "",
                    Subject = "Test Subject",
                    Body = "Test Body",
                    DateCreated = DateTime.Now,
                    DateSent = null,
                    Status = QueuedMessageStatus.Pending.ToString()
                },
                new QueuedMessage
                {
                    ID = Guid.NewGuid().ToString(),
                    FromAddress = "do.not.reply@ajtatum.com",
                    ToAddresses = "ben@ajtatum.com",
                    CCAddresses = "james@ajtatum.com,bentley@ajtatum.com",
                    BCCAddresses = "",
                    Subject = "Test Subject 2",
                    Body = "Test Body 2",
                    DateCreated = DateTime.Now,
                    DateSent = null,
                    Status = QueuedMessageStatus.Error.ToString()
                }
            };

            ContainerBuilder builder = new ContainerBuilder();
            builder.Register(x => this.settings.Object).As<IAppSettings>().InstancePerDependency();
            builder.Register(x => this.mailer.Object).As<IMailer>().InstancePerDependency();
            builder.Register(x => this.logger.Object).As<ICentralLogger>().InstancePerDependency();
            this.container = builder.Build();
        }

        [TestMethod]
        public async Task LogsFailure()
        {
            this.mailer.Setup(m => m.GetQueuedMessages()).Returns(this.testData);
            this.mailer.Setup(m => m.ProcessQueuedMessage(It.IsAny<QueuedMessage>())).Throws(new Exception("testing exception"));

            await this.winService.ProcessMessages(this.container, new System.Timers.Timer());

            this.logger.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [TestMethod]
        public async Task PassesProcessing()
        {
            this.mailer.Setup(m => m.GetQueuedMessages()).Returns(this.testData);
            this.settings.Setup(s => s.GetSetting<int>("MessageBunchSize")).Returns(1000);
            this.settings.Setup(s => s.GetSetting<int>("MessageBunchWaitMilliseconds")).Returns(1000);
            this.settings.Setup(s => s.GetSetting<int>("WaitBetweenMessagesMilliseconds")).Returns(0);

            await this.winService.ProcessMessages(this.container, new System.Timers.Timer());

            this.logger.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
        }
    }
}